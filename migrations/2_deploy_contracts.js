const PiCommodityTokenFacet = artifacts.require("PiCommodityTokenFacet");
const PiCommodityTokenDiamond = artifacts.require("PiCommodityTokenDiamond");
const PiFiatTokenFacet = artifacts.require("PiFiatTokenFacet");
const PiFiatTokenDiamond = artifacts.require("PiFiatTokenDiamond");

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000010";

module.exports = function(deployer, network, accounts) {
    deployer.deploy(PiCommodityTokenFacet, accounts[0]).then(function() {
        let array = [];
        array.push(PiCommodityTokenFacet.address);
        return deployer.deploy(PiCommodityTokenDiamond, "Commodity Name", "CNT", accounts[0], array);
    });
    deployer.deploy(PiFiatTokenFacet, accounts[0]).then(function() {
        let array = [];
        array.push(PiFiatTokenFacet.address);
        // accounts[0] and accounts[1] simulate the address of previous tokens 
        return deployer.deploy(PiFiatTokenDiamond, "Fiat Name", "FNT", accounts[0], "0", accounts[0], accounts[1], array);
    })
}