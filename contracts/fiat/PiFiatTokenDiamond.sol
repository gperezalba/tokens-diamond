pragma solidity ^0.5.0;

import "../utils/Owned.sol";
import "./PiFiatTokenStorage.sol";
import "./IDiamondFacet.sol";

contract PiFiatTokenDiamond is 
    PiFiatTokenStorage, 
    Owned 
{
    event DiamondCut(
        bytes4 indexed selector, 
        address indexed oldFacet, 
        address indexed newFacet
    );

    constructor(
        string memory name, 
        string memory symbol, 
        address _owner, 
        uint initialSupply, 
        address prevToken1,
        address prevToken2,
        address[] memory _facets
    ) 
        Owned(_owner) 
        public 
    {
        _name = name;
        _symbol = symbol;
        _decimals = 18;
        _prevToken1 = prevToken1;
        _prevToken2 = prevToken2;
        totalSupply = initialSupply;
        balances[_owner] = totalSupply;
        emisorAddress = address(0x0000000000000000000000000000000000000010);

        conversionAllowed = true;
        cuttable = true;
        _diamondCut(_facets);
    }

    function stopCuts() external onlyOwner {
        cuttable = false;
    }

    function diamondCut(address[] calldata _addresses) external onlyOwner {
        _diamondCut(_addresses);
    }

    function _diamondCut(address[] memory _addresses) internal {

        for (uint i = 0; i < _addresses.length; i++) {
            IDiamondFacet _facet = IDiamondFacet(_addresses[i]);
            bytes4[] memory _selectors = _facet.getSelectors();

            for (uint j = 0; j < _selectors.length; j++) {
                if (facets[_selectors[j]] != address(0)) {
                    require(cuttable, "Not cuttable");
                }

                emit DiamondCut(_selectors[j], facets[_selectors[j]], _addresses[i]);

                facets[_selectors[j]] = _addresses[i];
            }
        }
    }

    function () external payable {
        address _facet = facets[msg.sig];

        require(_facet != address(0), "Function not found");

        assembly {
          let ptr := mload(0x40)
          calldatacopy(ptr, 0, calldatasize())
          let result := delegatecall(gas(), _facet, ptr, calldatasize(), 0, 0)
          let size := returndatasize()
          returndatacopy(ptr, 0, size)
          switch result
          case 0 {revert(ptr, size)}
          default {return (ptr, size)}
        }
    }
}