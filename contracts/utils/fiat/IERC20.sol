pragma solidity ^0.5.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP. Does not include
 * the optional functions; to access them see `ERC20Detailed`.
 */
contract IERC20 {
    
    uint public totalSupply;
    function balanceOf(address who) public view returns (uint);

    function name() public view returns (string memory _name);
    function symbol() public view returns (string memory _symbol);
    function decimals() public view returns (uint8 _decimals);

    function transfer(address to, uint value) public;
    //function transferFrom (address _to, address payable _from) public;

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to `approve`. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
