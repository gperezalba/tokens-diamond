pragma solidity ^0.5.0;

import "../utils/Owned.sol";
import "../utils/safeMath.sol";
import "./PiFiatTokenStorage.sol";
import "./IDiamondFacet.sol";
import "../utils/fiat/ERC223_receiving_contract.sol";

contract DiamondCutExample is 
    PiFiatTokenStorage, 
    Owned,
    IDiamondFacet
{
    using SafeMath for uint;

    bytes4[] public selectors;

    event NewFunction();
    event NewTransfer();

    constructor() Owned(msg.sender) public {
        selectors.push(this.transfer.selector); //0xa9059cbb
        selectors.push(this.newFunction.selector); //0x1b28d63e
    }

    function getSelectors () external view returns (bytes4[] memory) {
        return selectors;
    }

    function newFunction() public {
        emit NewFunction();
    }

    function transfer(address _to, uint _value) public {
        _transfer(_to, msg.sender,_value);
        emit NewTransfer();
    }

    function _transfer(address _to, address payable _from, uint _value) internal {
        require(balances[_from] >= _value, "No balance");
        uint codeLength;
        bytes memory empty;

        assembly {
            // Retrieve the size of the code on target address, this needs assembly .
            codeLength := extcodesize(_to)
        }

        balances[_from] = balances[_from].sub(_value);
        balances[_to] = balances[_to].add(_value);
        if(codeLength>0) {
            ERC223ReceivingContract receiver = ERC223ReceivingContract(_to);
            receiver.tokenFallback(_from, _value);
        }
        //emit Transfer(_from, _to, _value);
        //emit Transfer(_from, _to, _value, empty);
    }
}