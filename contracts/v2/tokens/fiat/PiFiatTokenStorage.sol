pragma solidity ^0.5.0;

contract PiFiatTokenStorage {
    string internal _name;
    string internal _symbol;
    uint8 internal _decimals;
    uint public totalSupply;
    address public emisorAddress;
    //convertion
    address public _prevToken1;
    address public _prevToken2;
    bool public active;
    bool public conversionAllowed;
    //diamond
    bool public cuttable;
    bool public upgradable;

    mapping(address => uint) public balances;
    mapping(address => mapping (address => uint)) public approved;
    mapping(bytes4 => address) public facets;
}