pragma solidity ^0.5.15;

interface IDiamondFacet {
    function getSelectors () external view returns (bytes4[] memory);
} 