pragma solidity ^0.5.15;

contract PiCommodityTokenStorage {

    uint256 public globalId;

    /**
    * @dev Array of all NFT IDs.
    */
    string[] public tokens;

    /**
    * List of revert message codes. Implementing dApp should handle showing the correct message.
    * Based on 0xcert framework error codes.
    */
    string constant ZERO_ADDRESS = "003001";
    string constant NOT_VALID_NFT = "003002";
    string constant NOT_OWNER_OR_OPERATOR = "003003";
    string constant NOT_OWNER_APPROWED_OR_OPERATOR = "003004";
    string constant NOT_ABLE_TO_RECEIVE_NFT = "003005";
    string constant NFT_ALREADY_EXISTS = "003006";
    string constant NOT_OWNER = "003007";
    string constant IS_OWNER = "003008";

    /**
    * @dev Magic value of a smart contract that can recieve NFT.
    * Equal to: bytes4(keccak256("onERC721Received(address,address,uint256,bytes)")).
    */
    bytes4 internal constant MAGIC_ON_ERC721_RECEIVED = 0x150b7a02;

    /**
    * @dev Mapping from token ID to its index in global tokens array.
    */
    mapping(uint256 => uint256) internal idToIndex;

    /**
    * @dev A mapping from NFT ID to the address that owns it.
    */
    mapping (uint256 => address) internal idToOwner;

    /**
    * @dev Mapping from NFT ID to approved address.
    */
    mapping (uint256 => address) internal idToApproval;

    /**
    * @dev Mapping from owner address to count of his tokens.
    */
    mapping (address => uint256) internal ownerToNFTokenCount;

    /**
    * @dev Mapping from owner address to mapping of operator addresses.
    */
    mapping (address => mapping (address => bool)) internal ownerToOperators;

    /**
    * @dev Mapping from owner to list of owned NFT IDs.
    */
    mapping(address => string[]) internal ownerToIds;

    /**
    * @dev Mapping from NFT ID to its index in the owner tokens list.
    */
    mapping(uint256 => uint256) internal idToOwnerIndex;

    string constant INVALID_INDEX = "005007";
    string internal nftName;
    string internal nftSymbol;

    bool public cuttable;

    mapping (uint256 => string) internal idToRef;
    mapping (bytes32 => uint256) internal refToId;
    mapping (uint256 => string) internal idTojson;
    mapping(bytes4 => address) public facets;
}