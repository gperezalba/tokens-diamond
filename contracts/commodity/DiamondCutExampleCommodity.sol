pragma solidity ^0.5.0;

import "../utils/commodity/ERC721.sol";
import "../utils/commodity/ERC721TokenReceiver.sol";
import "../utils/Owned.sol";
import "../utils/safeMath.sol";
import "../utils/commodity/SupportsInterface.sol";
import "../utils/commodity/AddressUtils.sol";
import "./PiCommodityTokenStorage.sol";
import "./IDiamondFacet.sol";

contract DiamondCutExampleCommodity is 
    PiCommodityTokenStorage, 
    Owned,
    //ERC721,
    SupportsInterface, 
    IDiamondFacet 
{
    using SafeMath for uint;

    bytes4[] public selectors;

    modifier canOperate(
        uint256 _tokenId
    )
    {
        address tokenOwner = idToOwner[_tokenId];
        require(tokenOwner == msg.sender || ownerToOperators[tokenOwner][msg.sender], NOT_OWNER_OR_OPERATOR);
        _;
    }

    modifier canTransfer(
        uint256 _tokenId
    )
    {
        address tokenOwner = idToOwner[_tokenId];
        require(
            tokenOwner == msg.sender
            || idToApproval[_tokenId] == msg.sender
            || ownerToOperators[tokenOwner][msg.sender],
            NOT_OWNER_APPROWED_OR_OPERATOR
        );
        _;
    }

    modifier validNFToken(
        uint256 _tokenId
    )
    {
        require(idToOwner[_tokenId] != address(0), NOT_VALID_NFT);
        _;
    }

    event NewFunction();
    event NewTransfer();

    constructor() Owned(msg.sender) public {
        selectors.push(this.transferFrom.selector); //0xa9059cbb
        selectors.push(this.newFunction.selector); //0x1b28d63e
    }

    function getSelectors () external view returns (bytes4[] memory) {
        return selectors;
    }

    function newFunction() public {
        emit NewFunction();
    }

    function transferFrom(
        address _from,
        address _to,
        uint256 _tokenId
    )
        public
        canTransfer(_tokenId)
        validNFToken(_tokenId)
    {
        address tokenOwner = idToOwner[_tokenId];
        require(tokenOwner == _from, NOT_OWNER);
        require(_to != address(0), ZERO_ADDRESS);

        _transfer(_to, _tokenId);
    }

    function _transfer(
        address _to,
        uint256 _tokenId
    )
        internal
    {
        address from = idToOwner[_tokenId];
        _clearApproval(_tokenId);

        _removeNFToken(from, _tokenId);
        _addNFToken(_to, _tokenId);

        emit NewTransfer();
    }

    function _removeNFToken(
        address _from,
        uint256 _tokenId
    )
        internal
    {
        require(idToOwner[_tokenId] == _from, NOT_OWNER);
        ownerToNFTokenCount[_from] = ownerToNFTokenCount[_from].sub(1);
        delete idToOwner[_tokenId];

        uint256 tokenToRemoveIndex = idToOwnerIndex[_tokenId];
        uint256 lastTokenIndex = ownerToIds[_from].length - 1;

        if (lastTokenIndex != tokenToRemoveIndex)
        {
            string memory lastToken = ownerToIds[_from][lastTokenIndex];
            ownerToIds[_from][tokenToRemoveIndex] = lastToken;
            bytes32 lastTokenRef = keccak256(abi.encodePacked(lastToken));
            idToOwnerIndex[refToId[lastTokenRef]] = tokenToRemoveIndex;
        }

        ownerToIds[_from].pop();
    }

    function _addNFToken(
        address _to,
        uint256 _tokenId
    )
        internal
        //virtual
    {
        require(idToOwner[_tokenId] == address(0), NFT_ALREADY_EXISTS);

        idToOwner[_tokenId] = _to;
        ownerToNFTokenCount[_to] = ownerToNFTokenCount[_to].add(1);

        ownerToIds[_to].push(idToRef[_tokenId]);
        idToOwnerIndex[_tokenId] = ownerToIds[_to].length - 1;
    }

    function _clearApproval(
        uint256 _tokenId
    )
        private
    {
        if (idToApproval[_tokenId] != address(0))
        {
            delete idToApproval[_tokenId];
        }
    }
}