pragma solidity ^0.5.15;

import "./PiCommodityTokenFacet.sol";
import "./IDiamondFacet.sol";
import "./PiCommodityTokenStorage.sol";

contract PiCommodityTokenDiamond is 
    PiCommodityTokenStorage, 
    Owned 
{

    event DiamondCut(
        bytes4 indexed selector, 
        address indexed oldFacet, 
        address indexed newFacet
    );

    constructor(
        string memory _name, 
        string memory _symbol,
        address _owner,
        address[] memory _facets
    ) 
        Owned(_owner) 
        public 
    {
        nftName = _name;
        nftSymbol = _symbol;

        cuttable = true;
        _diamondCut(_facets);
    }

    function stopCuts() external onlyOwner {
        cuttable = false;
    }

    function diamondCut(address[] calldata _addresses) external onlyOwner {
        _diamondCut(_addresses);
    }

    function _diamondCut(address[] memory _addresses) internal {

        for (uint i = 0; i < _addresses.length; i++) {
            IDiamondFacet _facet = IDiamondFacet(_addresses[i]);
            bytes4[] memory _selectors = _facet.getSelectors();

            for (uint j = 0; j < _selectors.length; j++) {
                if (facets[_selectors[j]] != address(0)) {
                    require(cuttable, "Not cuttable");
                }

                emit DiamondCut(_selectors[j], facets[_selectors[j]], _addresses[i]);

                facets[_selectors[j]] = _addresses[i];
            }
        }
    }

    function () external payable {
        address _facet = facets[msg.sig];

        require(_facet != address(0), "Function not found");

        assembly {
          let ptr := mload(0x40)
          calldatacopy(ptr, 0, calldatasize())
          let result := delegatecall(gas(), _facet, ptr, calldatasize(), 0, 0)
          let size := returndatasize()
          returndatacopy(ptr, 0, size)
          switch result
          case 0 {revert(ptr, size)}
          default {return (ptr, size)}
        }
    }
}