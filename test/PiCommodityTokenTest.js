const expect = require("chai").expect;
const truffleAssert = require('truffle-assertions');
const sha3 = require('js-sha3').keccak_256;
const BigNumber = require('bignumber.js');
const BN = web3.utils.BN;

require('chai')
  .use(require('chai-bignumber')(BN))
  .should();

const PiCommodityTokenFacet = artifacts.require("PiCommodityTokenFacet");
const PiCommodityTokenDiamond = artifacts.require("PiCommodityTokenDiamond");
const DiamondCutExampleCommodity = artifacts.require("DiamondCutExampleCommodity");

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";
const ONE = "1";
const TEN = "10";
const HUNDRED = "100";
const TOKEN_REFS = ["REF01", "REF02", "REF03", "REF04", "REF05"];
const TOKEN_JSONS = ["{'peso':1000,'ley':91}", "{'peso':2000,'ley':92}", "{'peso':3000,'ley':93}", "{'peso':4000,'ley':94}", "{'peso':5000,'ley':95}"];

contract("PiCommodityTokenDiamond", async (accounts) => {

    it("should set a new Owner", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let oldOwner = await diamond.owner.call();
        let response = await diamond.setOwner(accounts[1], {from: accounts[0]});
        let newOwner = await diamond.owner.call();

        expect(oldOwner).to.equal(accounts[0]);
        expect(newOwner).to.equal(accounts[1]);

        truffleAssert.eventEmitted(response, 'NewOwner', (ev) => {
            return ev.old == accounts[0] && ev.current == accounts[1];
        });
    })

    it("should revert on set new Owner from no-owner", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        await truffleAssert.reverts(diamond.setOwner(accounts[1], {from: accounts[0]}));
    })

    it("should return name", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.name().encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('string', response);
        
        expect(decoded).to.equal("Commodity Name");
    })

    it("should return symbol", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.symbol().encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('string', response);
        
        expect(decoded).to.equal("CNT");
    })

    it("should return 0 balance", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.balanceOf(accounts[2]).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);
        
        expect(decoded).to.equal("0");
    })

    it("should return 0 totalSupply", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.totalSupply().encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);
        
        expect(decoded).to.equal("0");
    })

    it("should mint tokens", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.mint(accounts[2], TOKEN_REFS[0], TOKEN_JSONS[0]).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0, gas: 1000000});
        
        let data1 = facet.methods.balanceOf(accounts[2]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balance = web3.eth.abi.decodeParameter('uint256', response1);

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiCommodityTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer._from;
        let to = events.Transfer._to;
        let tokenId = events.Transfer._tokenId;

        
        let data2 = facet.methods.ownerOf(tokenId).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let owner = web3.eth.abi.decodeParameter('address', response2);

        let data3 = facet.methods.ownerOfRef(TOKEN_REFS[0]).encodeABI();
        let response3 = await web3.eth.call({to: diamond.address, data: data3});
        let owner2 = web3.eth.abi.decodeParameter('address', response3);
        
        expect(from).to.equal(ZERO_ADDRESS);
        expect(to).to.equal(accounts[2]);
        expect(tokenId).to.equal("1");
        expect(balance).to.equal("1");
        expect(owner).to.equal(accounts[2]);
        expect(owner2).to.equal(accounts[2]);
    })

    it("should return 1 totalSupply", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.totalSupply().encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);
        
        expect(decoded).to.equal("1");
    })

    it("should return metadata by id", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.getMetadata(1).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('string', response);
        
        expect(decoded).to.equal(TOKEN_JSONS[0]);
    })

    it("should return metadata by ref", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.getMetadataRef(TOKEN_REFS[0]).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('string', response);
        
        expect(decoded).to.equal(TOKEN_JSONS[0]);
    })

    it("should return tokenInfo by id", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.getTokenInfo(1).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameters(['address', 'string'], response);
        
        expect(decoded[0]).to.equal(accounts[2]);
        expect(decoded[1]).to.equal(TOKEN_JSONS[0]);
    })

    it("should return tokenInfo by ref", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.getTokenInfoRef(TOKEN_REFS[0]).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameters(['address', 'string'], response);
        
        expect(decoded[0]).to.equal(accounts[2]);
        expect(decoded[1]).to.equal(TOKEN_JSONS[0]);
    })

    it("should mint multiple tokens", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.mint(accounts[3], TOKEN_REFS[1], TOKEN_JSONS[1]).encodeABI();
        await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0, gas: 1000000});

        data = facet.methods.mint(accounts[3], TOKEN_REFS[2], TOKEN_JSONS[2]).encodeABI();
        await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0, gas: 1000000});
        
        data = facet.methods.mint(accounts[3], TOKEN_REFS[3], TOKEN_JSONS[3]).encodeABI();
        await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0, gas: 1000000});

        let data3 = facet.methods.tokenOfOwnerByIndex(accounts[3], "0").encodeABI();
        let response3 = await web3.eth.call({to: diamond.address, data: data3});
        let ref1 = web3.eth.abi.decodeParameter('string', response3);

        let data4 = facet.methods.tokenOfOwnerByIndex(accounts[3], "1").encodeABI();
        let response4 = await web3.eth.call({to: diamond.address, data: data4});
        let ref2 = web3.eth.abi.decodeParameter('string', response4);

        let data5 = facet.methods.tokenOfOwnerByIndex(accounts[3], "2").encodeABI();
        let response5 = await web3.eth.call({to: diamond.address, data: data5});
        let ref3 = web3.eth.abi.decodeParameter('string', response5);

        let data6 = facet.methods.tokenOfOwner(accounts[3]).encodeABI();
        let response6 = await web3.eth.call({to: diamond.address, data: data6});
        let refs = web3.eth.abi.decodeParameter('string[]', response6);

        expect(ref1).to.equal(TOKEN_REFS[1]);
        expect(ref2).to.equal(TOKEN_REFS[2]);
        expect(ref3).to.equal(TOKEN_REFS[3]);
        expect(refs[0]).to.equal(ref1);
        expect(refs[1]).to.equal(ref2);
        expect(refs[2]).to.equal(ref3);
    })

    it("should return 4 totalSupply", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.totalSupply().encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);
        
        expect(decoded).to.equal("4");
    })

    it("should return metadata again to check integrity", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.getMetadataRef(TOKEN_REFS[0]).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('string', response);
        
        expect(decoded).to.equal(TOKEN_JSONS[0]);
    })

    it("should return tokenInfo again to check integrity", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.getTokenInfo(1).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameters(['address', 'string'], response);
        
        expect(decoded[0]).to.equal(accounts[2]);
        expect(decoded[1]).to.equal(TOKEN_JSONS[0]);
    })

    it("should return array of tokens", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.getTokens().encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('string[]', response);
        
        expect(decoded[0]).to.equal(TOKEN_REFS[0]);
        expect(decoded[1]).to.equal(TOKEN_REFS[1]);
        expect(decoded[2]).to.equal(TOKEN_REFS[2]);
        expect(decoded[3]).to.equal(TOKEN_REFS[3]);
    })

    it("should safeTransferFrom tokens", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);

        let data1 = facet.methods.balanceOf(accounts[2]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePre1 = web3.eth.abi.decodeParameter('uint256', response1);

        let data2 = facet.methods.balanceOf(accounts[4]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let balancePre2 = web3.eth.abi.decodeParameter('uint256', response2);

        let data = facet.methods.safeTransferFrom(accounts[2], accounts[4], 1, '0x').encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[2], to: diamond.address, data: data, value: 0, gas: 1000000});

        response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePost1 = web3.eth.abi.decodeParameter('uint256', response1);

        response2 = await web3.eth.call({to: diamond.address, data: data2});
        let balancePost2 = web3.eth.abi.decodeParameter('uint256', response2);

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiCommodityTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer._from;
        let to = events.Transfer._to;
        let tokenId = events.Transfer._tokenId;

        let data3 = facet.methods.ownerOf(tokenId).encodeABI();
        let response3 = await web3.eth.call({to: diamond.address, data: data3});
        let owner = web3.eth.abi.decodeParameter('address', response3);
        
        expect(from).to.equal(accounts[2]);
        expect(to).to.equal(accounts[4]);
        expect(tokenId).to.equal("1");
        expect(balancePre1).to.equal("1");
        expect(balancePre2).to.equal("0");
        expect(balancePost1).to.equal("0");
        expect(balancePost2).to.equal("1");
        expect(owner).to.equal(accounts[4]);
    })

    it("should set array tokens owned by owner to empty", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);

        let data = facet.methods.tokenOfOwner(accounts[2]).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let refs = web3.eth.abi.decodeParameter('string[]', response);

        expect(refs.length).to.equal(0);
    })

    it("should safeTransferFromRef tokens when owns multiple tokens", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);

        let data1 = facet.methods.balanceOf(accounts[3]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePre1 = web3.eth.abi.decodeParameter('uint256', response1);

        let data2 = facet.methods.balanceOf(accounts[4]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let balancePre2 = web3.eth.abi.decodeParameter('uint256', response2);

        let data3 = facet.methods.tokenOfOwner(accounts[3]).encodeABI();
        let response3 = await web3.eth.call({to: diamond.address, data: data3});
        let refsPre = web3.eth.abi.decodeParameter('string[]', response3);

        let data = facet.methods.safeTransferFromRef(accounts[3], accounts[4], TOKEN_REFS[2], '0x').encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[3], to: diamond.address, data: data, value: 0, gas: 1000000});

        response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePost1 = web3.eth.abi.decodeParameter('uint256', response1);

        response2 = await web3.eth.call({to: diamond.address, data: data2});
        let balancePost2 = web3.eth.abi.decodeParameter('uint256', response2);

        response3 = await web3.eth.call({to: diamond.address, data: data3});
        let refsPost = web3.eth.abi.decodeParameter('string[]', response3);

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiCommodityTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer._from;
        let to = events.Transfer._to;
        let tokenId = events.Transfer._tokenId;

        let data4 = facet.methods.ownerOf(tokenId).encodeABI();
        let response4 = await web3.eth.call({to: diamond.address, data: data4});
        let owner = web3.eth.abi.decodeParameter('address', response4);
        
        expect(from).to.equal(accounts[3]);
        expect(to).to.equal(accounts[4]);
        expect(tokenId).to.equal("3");
        expect(balancePre1).to.equal(String(parseInt(balancePost1) + 1));
        expect(balancePre2).to.equal(String(parseInt(balancePost2) - 1));
        expect(owner).to.equal(accounts[4]);

        let findPre = refsPre.findIndex(function(el) {
            return el == TOKEN_REFS[2];
        });
        let last = refsPre[refsPre.length - 1]
        let findLastPre = refsPre.findIndex(function(el) {
            return el == last;
        });
        let findPost = refsPost.findIndex(function(el) {
            return el == TOKEN_REFS[2];
        });
        let findLastPost = refsPost.findIndex(function(el) {
            return el == last;
        });

        expect(findPre).to.equal(1);
        expect(findPost).to.equal(-1); //not in array
        expect(findLastPost).to.equal(findPre); //last goes to deleted's position
        expect(findLastPre).to.equal(2);
    })

    it("should approve", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.approve(accounts[5], 1).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[4], to: diamond.address, data: data, value: 0, gas: 1000000});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiCommodityTokenFacet, log);
            events[result[1]] = result[0];
        })

        let owner = events.Approval._owner;
        let approved = events.Approval._approved;
        let tokenId = events.Approval._tokenId;

        let data1 = facet.methods.getApproved(1).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('address', response1);

        expect(owner).to.equal(accounts[4]);
        expect(approved).to.equal(accounts[5]);
        expect(tokenId).to.equal('1');
        expect(decoded1).to.equal(accounts[5]);
    })

    it("should transferFrom tokens with approved account", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);

        let data1 = facet.methods.balanceOf(accounts[4]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePre1 = web3.eth.abi.decodeParameter('uint256', response1);

        let data2 = facet.methods.balanceOf(accounts[6]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let balancePre2 = web3.eth.abi.decodeParameter('uint256', response2);

        let data = facet.methods.transferFrom(accounts[4], accounts[6], 1).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[5], to: diamond.address, data: data, value: 0, gas: 1000000});

        response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePost1 = web3.eth.abi.decodeParameter('uint256', response1);

        response2 = await web3.eth.call({to: diamond.address, data: data2});
        let balancePost2 = web3.eth.abi.decodeParameter('uint256', response2);

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiCommodityTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer._from;
        let to = events.Transfer._to;
        let tokenId = events.Transfer._tokenId;

        let data3 = facet.methods.ownerOf(tokenId).encodeABI();
        let response3 = await web3.eth.call({to: diamond.address, data: data3});
        let owner = web3.eth.abi.decodeParameter('address', response3);
        
        expect(from).to.equal(accounts[4]);
        expect(to).to.equal(accounts[6]);
        expect(tokenId).to.equal("1");
        expect(balancePre1).to.equal(String(parseInt(balancePost1) + 1));
        expect(balancePre2).to.equal(String(parseInt(balancePost2) - 1));
        expect(owner).to.equal(accounts[6]);
    })

    it("should reset approve after transfer", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);

        let data1 = facet.methods.getApproved(1).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('address', response1);

        expect(decoded1).to.equal(ZERO_ADDRESS);
    })

    it("should setApprovalForAll", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.setApprovalForAll(accounts[5], true).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[6], to: diamond.address, data: data, value: 0, gas: 1000000});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiCommodityTokenFacet, log);
            events[result[1]] = result[0];
        })

        let owner = events.ApprovalForAll._owner;
        let operator = events.ApprovalForAll._operator;
        let approved = events.ApprovalForAll._approved;

        let data1 = facet.methods.isApprovedForAll(accounts[6], accounts[5]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('bool', response1);

        expect(owner).to.equal(accounts[6]);
        expect(operator).to.equal(accounts[5]);
        expect(approved).to.equal(true);
        expect(decoded1).to.equal(true);
    })

    it("should approveRef from operator account", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.approveRef(accounts[7], TOKEN_REFS[0]).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[5], to: diamond.address, data: data, value: 0, gas: 1000000});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiCommodityTokenFacet, log);
            events[result[1]] = result[0];
        })

        let owner = events.Approval._owner;
        let approved = events.Approval._approved;
        let tokenId = events.Approval._tokenId;

        let data1 = facet.methods.getApproved(1).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('address', response1);

        expect(owner).to.equal(accounts[6]);
        expect(approved).to.equal(accounts[7]);
        expect(tokenId).to.equal('1');
        expect(decoded1).to.equal(accounts[7]);
    })

    it("should transferFromRef tokens with operator account", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);

        let data1 = facet.methods.balanceOf(accounts[6]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePre1 = web3.eth.abi.decodeParameter('uint256', response1);

        let data2 = facet.methods.balanceOf(accounts[8]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let balancePre2 = web3.eth.abi.decodeParameter('uint256', response2);

        let data = facet.methods.transferFromRef(accounts[6], accounts[8], TOKEN_REFS[0]).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[5], to: diamond.address, data: data, value: 0, gas: 1000000});

        response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePost1 = web3.eth.abi.decodeParameter('uint256', response1);

        response2 = await web3.eth.call({to: diamond.address, data: data2});
        let balancePost2 = web3.eth.abi.decodeParameter('uint256', response2);

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiCommodityTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer._from;
        let to = events.Transfer._to;
        let tokenId = events.Transfer._tokenId;

        let data3 = facet.methods.ownerOf(tokenId).encodeABI();
        let response3 = await web3.eth.call({to: diamond.address, data: data3});
        let owner = web3.eth.abi.decodeParameter('address', response3);
        
        expect(from).to.equal(accounts[6]);
        expect(to).to.equal(accounts[8]);
        expect(tokenId).to.equal("1");
        expect(balancePre1).to.equal(String(parseInt(balancePost1) + 1));
        expect(balancePre2).to.equal(String(parseInt(balancePost2) - 1));
        expect(owner).to.equal(accounts[8]);
    })

    it("should reset approve after transfer with operator", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);

        let data1 = facet.methods.getApproved(1).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('address', response1);

        expect(decoded1).to.equal(ZERO_ADDRESS);
    })

    it("should burn", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);

        let data1 = facet.methods.balanceOf(accounts[8]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePre1 = web3.eth.abi.decodeParameter('uint256', response1);

        let data = facet.methods.burn(1).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[8], to: diamond.address, data: data, value: 0, gas: 1000000});

        response1 = await web3.eth.call({to: diamond.address, data: data1});
        let balancePost1 = web3.eth.abi.decodeParameter('uint256', response1);

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiCommodityTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer._from;
        let to = events.Transfer._to;
        let tokenId = events.Transfer._tokenId;
        
        expect(from).to.equal(accounts[8]);
        expect(to).to.equal(ZERO_ADDRESS);
        expect(tokenId).to.equal("1");
        expect(balancePre1).to.equal(String(parseInt(balancePost1) + 1));
    })

    it("should revert safeTransferFrom from no-owner/approved/operator", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.safeTransferFrom(accounts[2], accounts[4], 1, '0x').encodeABI();
        await truffleAssert.reverts(web3.eth.sendTransaction({from: accounts[0], to: diamond.address, data: data, value: 0, gas: 1000000}));
    })

    it("should revert transferFrom from no-owner/approved/operator", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.transferFrom(accounts[2], accounts[4], 1).encodeABI();
        await truffleAssert.reverts(web3.eth.sendTransaction({from: accounts[0], to: diamond.address, data: data, value: 0, gas: 1000000}));
    })

    it("should revert approve from no-owner/operator", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.approve(accounts[2], 1).encodeABI();
        await truffleAssert.reverts(web3.eth.sendTransaction({from: accounts[0], to: diamond.address, data: data, value: 0, gas: 1000000}));
    })

    it("should revert burn from no-owner/approved/operator", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);
        let data = facet.methods.burn(1).encodeABI();
        await truffleAssert.reverts(web3.eth.sendTransaction({from: accounts[0], to: diamond.address, data: data, value: 0, gas: 1000000}));
    })

    it("should create a new diamondCut", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let newFacet = await DiamondCutExampleCommodity.new();
        let array = [];
        array.push(newFacet.address);
        let response = await diamond.diamondCut(array, {from: accounts[1]});

        let selector1 = response.receipt.logs[0].args.selector;
        selector1 = web3.eth.abi.decodeParameter('bytes4', selector1);

        let selector2 = response.receipt.logs[1].args.selector;
        selector2 = web3.eth.abi.decodeParameter('bytes4', selector2);

        let oldFacet1 = response.receipt.logs[0].args.oldFacet;
        let oldFacet2 = response.receipt.logs[1].args.oldFacet;
        let newFacet1 = response.receipt.logs[0].args.newFacet;
        let newFacet2 = response.receipt.logs[1].args.newFacet;

        expect(oldFacet1).to.equal(PiCommodityTokenFacet.address);
        expect(oldFacet2).to.equal(ZERO_ADDRESS);
        expect(newFacet1).to.equal(newFacet.address);
        expect(newFacet2).to.equal(newFacet.address);
        expect(selector1).to.equal('0x23b872dd');
        expect(selector2).to.equal('0x1b28d63e');
        
    })

    it("should transfer with new function", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiCommodityTokenFacet.abi, PiCommodityTokenFacet.address);

        let data1 = facet.methods.balanceOf(accounts[3]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('uint256', response1);

        let data = facet.methods.transferFrom(accounts[3], accounts[2], 2).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[3], to: diamond.address, data: data, value: 0, gas: 1000000});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(DiamondCutExampleCommodity, log);
            events[result[1]] = result[0];
        })

        let data2 = facet.methods.balanceOf(accounts[3]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('uint256', response2);
        
        expect(decoded1).to.equal(String(parseInt(decoded2) + parseInt(ONE)));
        expect(result[1]).to.equal('NewTransfer');
    })

    it("should delegate with new function", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let newFacet = new web3.eth.Contract(DiamondCutExampleCommodity.abi);
        let data = newFacet.methods.newFunction().encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[2], to: diamond.address, data: data, value: 0});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(DiamondCutExampleCommodity, log);
            events[result[1]] = result[0];
        })

        expect(result[1]).to.equal('NewFunction');
    })

    it("should stopCuts", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let bool1 = await diamond.cuttable.call();
        await diamond.stopCuts({from: accounts[1]});
        let bool2 = await diamond.cuttable.call();

        expect(bool1).to.equal(true);
        expect(bool2).to.equal(false);
    })

    it("should revert on diamondCut after stopCuts", async () => {
        let diamond = await PiCommodityTokenDiamond.deployed();
        let newFacet = await DiamondCutExampleCommodity.new();
        let array = [];
        array.push(newFacet.address);
        await truffleAssert.reverts(diamond.diamondCut(array, {from: accounts[1]}));
    })
    
});

function decodeEvent(contract, log) {
	let inputs = contract.events[log.topics[0]].inputs;
	let data = log.data;
	let topics = log.topics.slice(1);
	let event = web3.eth.abi.decodeLog(inputs, data, topics);
	let name = contract.events[log.topics[0]].name;

	return [event, name];
}