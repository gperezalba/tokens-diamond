const expect = require("chai").expect;
const truffleAssert = require('truffle-assertions');
const sha3 = require('js-sha3').keccak_256;
const BigNumber = require('bignumber.js');
const BN = web3.utils.BN;

const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";
const ONE = "1";
const TEN = "10";
const HUNDRED = "100";

require('chai')
  .use(require('chai-bignumber')(BN))
  .should();

const PiFiatTokenFacet = artifacts.require("PiFiatTokenFacet");
const PiFiatTokenDiamond = artifacts.require("PiFiatTokenDiamond");
const DiamondCutExample = artifacts.require("DiamondCutExample");

contract("PiFiatTokenDiamond", async (accounts) => {

    it("should set a new Owner", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let oldOwner = await diamond.owner.call();
        let response = await diamond.setOwner(accounts[1], {from: accounts[0]});
        let newOwner = await diamond.owner.call();

        expect(oldOwner).to.equal(accounts[0]);
        expect(newOwner).to.equal(accounts[1]);

        truffleAssert.eventEmitted(response, 'NewOwner', (ev) => {
            return ev.old == accounts[0] && ev.current == accounts[1];
        });
    })

    it("should revert on set new Owner from no-owner", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        await truffleAssert.reverts(diamond.setOwner(accounts[1], {from: accounts[0]}));
    })

    it("should return name", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.name().encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('string', response);
        
        expect(decoded).to.equal("Fiat Name");
    })

    it("should return symbol", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.symbol().encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('string', response);
        
        expect(decoded).to.equal("FNT");
    })

    it("should return decimals", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.decimals().encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);
        
        expect(decoded).to.equal("18");
    })

    it("should return 0 balance", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.balanceOf(accounts[2]).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);
        
        expect(decoded).to.equal("0");
    })

    it("should mint tokens", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.mint(accounts[2], HUNDRED).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiFiatTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer.from;
        let to = events.Transfer.to;
        let value = events.Transfer.value;
        
        expect(from).to.equal(ZERO_ADDRESS);
        expect(to).to.equal(accounts[2]);
        expect(value).to.equal(HUNDRED);
    })

    it("should return minted balance", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.balanceOf(accounts[2]).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);
        
        expect(decoded).to.equal(HUNDRED);
    })

    it("should transfer tokens", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.transfer(accounts[3], TEN).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[2], to: diamond.address, data: data, value: 0});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiFiatTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer.from;
        let to = events.Transfer.to;
        let value = events.Transfer.value;
        
        expect(from).to.equal(accounts[2]);
        expect(to).to.equal(accounts[3]);
        expect(value).to.equal(TEN);
    })

    it("should revert on transfer tokens when no balance", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.transfer(accounts[3], ONE).encodeABI();
        await truffleAssert.reverts(web3.eth.sendTransaction({from: accounts[6], to: diamond.address, data: data, value: 0}));
    })

    it("should update balance after transfer", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.balanceOf(accounts[2]).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);

        let data2 = facet.methods.balanceOf(accounts[3]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('uint256', response2);
        
        expect(decoded).to.equal(String(parseInt(HUNDRED) - parseInt(TEN)));
        expect(decoded2).to.equal(TEN);
    })

    it("should approve address", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.approve(accounts[4], TEN).encodeABI();
        await web3.eth.sendTransaction({from: accounts[2], to: diamond.address, data: data, value: 0});

        let data2 = facet.methods.approved(accounts[2], accounts[4] ).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded = web3.eth.abi.decodeParameter('uint256', response2);
        
        expect(decoded).to.equal(TEN);
    })

    it("should transferFrom", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.transferFrom(accounts[4], accounts[2]).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiFiatTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer.from;
        let to = events.Transfer.to;
        let value = events.Transfer.value;
        
        expect(from).to.equal(accounts[2]);
        expect(to).to.equal(accounts[4]);
        expect(value).to.equal(TEN);
    })

    it("should revert on transferFrom when not approved", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.transferFrom(accounts[4], accounts[2]).encodeABI();
        await truffleAssert.reverts(web3.eth.sendTransaction({from: accounts[6], to: diamond.address, data: data, value: 0}));
    })

    it("should reset approved after transferFrom", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.approved(accounts[2], accounts[4] ).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);
        
        expect(decoded).to.equal("0");
    })

    it("should update balances after transferFrom", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.balanceOf(accounts[2]).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);

        let data2 = facet.methods.balanceOf(accounts[4]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('uint256', response2);
        
        expect(decoded).to.equal(String(parseInt(HUNDRED) - 2 * parseInt(TEN)));
        expect(decoded2).to.equal(TEN);
    })

    it("should transferFromValue", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.approve(accounts[5], TEN).encodeABI();
        await web3.eth.sendTransaction({from: accounts[4], to: diamond.address, data: data, value: 0});

        let data2 = facet.methods.transferFromValue(accounts[5], accounts[4], ONE).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data2, value: 0});
        
        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiFiatTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer.from;
        let to = events.Transfer.to;
        let value = events.Transfer.value;

        expect(from).to.equal(accounts[4]);
        expect(to).to.equal(accounts[5]);
        expect(value).to.equal(ONE);
    })

    it("should update approved after transferFromValue", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.approved(accounts[4], accounts[5] ).encodeABI();
        let response = await web3.eth.call({to: diamond.address, data: data});
        let decoded = web3.eth.abi.decodeParameter('uint256', response);
        
        expect(decoded).to.equal(String(parseInt(TEN) - 1));
    })

    it("should disapprove", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        
        let data1 = facet.methods.approved(accounts[4], accounts[5] ).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('uint256', response1);
        
        let data = facet.methods.disapprove(accounts[5]).encodeABI();
        await web3.eth.sendTransaction({from: accounts[4], to: diamond.address, data: data, value: 0});

        let data2 = facet.methods.approved(accounts[4], accounts[5] ).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('uint256', response2);
        
        expect(decoded1).to.equal(String(parseInt(TEN) - 1));
        expect(decoded2).to.equal("0");
    })

    it("should mint tokens to owner to burn them after", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.mint(accounts[1], HUNDRED).encodeABI();
        await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0});
    })

    it("should burn tokens", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        
        let data1 = facet.methods.balanceOf(accounts[1]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('uint256', response1);
        
        let data = facet.methods.burn(decoded1).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiFiatTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer.from;
        let to = events.Transfer.to;
        let value = events.Transfer.value;

        let data2 = facet.methods.balanceOf(accounts[1]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('uint256', response2);
        
        expect(decoded1).to.equal(HUNDRED);
        expect(decoded2).to.equal("0");
        expect(from).to.equal(accounts[1]);
        expect(to).to.equal(ZERO_ADDRESS);
        expect(value).to.equal(HUNDRED);
    })

    //accounts[0] and accounts[1] simulate address of old tokens so a call to function 
    //tokenFallback simulates a transfer from prevTokens to conversion
    it("should revert trying to convert before activation", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);

        let data = facet.methods.tokenFallback(accounts[1], ONE).encodeABI();
        await truffleAssert.reverts(web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0}));
    })

    it("should activate conversion", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);

        let data1 = facet.methods.active().encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('bool', response1);

        let data = facet.methods.activate().encodeABI();
        await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0});

        let data2 = facet.methods.active().encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('bool', response2);

        expect(decoded1).to.equal(false);
        expect(decoded2).to.equal(true);
    })

    it("should convert with pseudo-old-token-1", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);

        let data1 = facet.methods.balanceOf(accounts[6]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('uint256', response1);

        let data = facet.methods.tokenFallback(accounts[6], ONE).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[0], to: diamond.address, data: data, value: 0});
        
        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiFiatTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer.from;
        let to = events.Transfer.to;
        let value = events.Transfer.value;

        let data2 = facet.methods.balanceOf(accounts[6]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('uint256', response2);

        expect(String(parseInt(decoded1) + parseInt(ONE))).to.equal(decoded2);
        expect(from).to.equal(ZERO_ADDRESS);
        expect(to).to.equal(accounts[6]);
        expect(value).to.equal(ONE);
    })

    it("should convert with pseudo-old-token-2", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);

        let data1 = facet.methods.balanceOf(accounts[6]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('uint256', response1);

        let data = facet.methods.tokenFallback(accounts[6], ONE).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0});
        
        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(PiFiatTokenFacet, log);
            events[result[1]] = result[0];
        })

        let from = events.Transfer.from;
        let to = events.Transfer.to;
        let value = events.Transfer.value;

        let data2 = facet.methods.balanceOf(accounts[6]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('uint256', response2);

        expect(String(parseInt(decoded1) + parseInt(ONE))).to.equal(decoded2);
        expect(from).to.equal(ZERO_ADDRESS);
        expect(to).to.equal(accounts[6]);
        expect(value).to.equal(ONE);
    })

    it("should revert with FAKE-pseudo-old-token-3", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.tokenFallback(accounts[6], ONE).encodeABI();
        await truffleAssert.reverts(web3.eth.sendTransaction({from: accounts[2], to: diamond.address, data: data, value: 0}));
    })

    it("should finish conversion", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);

        let data1 = facet.methods.conversionAllowed().encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('bool', response1);

        let data = facet.methods.finishConversion().encodeABI();
        await web3.eth.sendTransaction({from: accounts[1], to: diamond.address, data: data, value: 0});

        let data2 = facet.methods.conversionAllowed().encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('bool', response2);

        expect(decoded1).to.equal(true);
        expect(decoded2).to.equal(false);
    })

    it("should revert conversion after having finish it", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);
        let data = facet.methods.tokenFallback(accounts[6], ONE).encodeABI();
        await truffleAssert.reverts(web3.eth.sendTransaction({from: accounts[0], to: diamond.address, data: data, value: 0}));
    })

    it("should create a new diamondCut", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let newFacet = await DiamondCutExample.new();
        let array = [];
        array.push(newFacet.address);
        let response = await diamond.diamondCut(array, {from: accounts[1]});

        let selector1 = response.receipt.logs[0].args.selector;
        selector1 = web3.eth.abi.decodeParameter('bytes4', selector1);

        let selector2 = response.receipt.logs[1].args.selector;
        selector2 = web3.eth.abi.decodeParameter('bytes4', selector2);

        let oldFacet1 = response.receipt.logs[0].args.oldFacet;
        let oldFacet2 = response.receipt.logs[1].args.oldFacet;
        let newFacet1 = response.receipt.logs[0].args.newFacet;
        let newFacet2 = response.receipt.logs[1].args.newFacet;

        expect(oldFacet1).to.equal(PiFiatTokenFacet.address);
        expect(oldFacet2).to.equal(ZERO_ADDRESS);
        expect(newFacet1).to.equal(newFacet.address);
        expect(newFacet2).to.equal(newFacet.address);
        expect(selector1).to.equal('0xa9059cbb');
        expect(selector2).to.equal('0x1b28d63e');
    })

    it("should transfer with new function", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let facet = new web3.eth.Contract(PiFiatTokenFacet.abi, PiFiatTokenFacet.address);

        let data1 = facet.methods.balanceOf(accounts[2]).encodeABI();
        let response1 = await web3.eth.call({to: diamond.address, data: data1});
        let decoded1 = web3.eth.abi.decodeParameter('uint256', response1);

        let data = facet.methods.transfer(accounts[3], ONE).encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[2], to: diamond.address, data: data, value: 0});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(DiamondCutExample, log);
            events[result[1]] = result[0];
        })

        let data2 = facet.methods.balanceOf(accounts[2]).encodeABI();
        let response2 = await web3.eth.call({to: diamond.address, data: data2});
        let decoded2 = web3.eth.abi.decodeParameter('uint256', response2);
        
        expect(decoded1).to.equal(String(parseInt(decoded2) + parseInt(ONE)));
        expect(result[1]).to.equal('NewTransfer');
    })

    it("should delegate with new function", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let newFacet = new web3.eth.Contract(DiamondCutExample.abi);
        let data = newFacet.methods.newFunction().encodeABI();
        let response = await web3.eth.sendTransaction({from: accounts[2], to: diamond.address, data: data, value: 0});

        let events = {};
        let result;

        response.logs.forEach(function (log) {
            result = decodeEvent(DiamondCutExample, log);
            events[result[1]] = result[0];
        })

        expect(result[1]).to.equal('NewFunction');
    })

    it("should stopCuts", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let bool1 = await diamond.cuttable.call();
        await diamond.stopCuts({from: accounts[1]});
        let bool2 = await diamond.cuttable.call();

        expect(bool1).to.equal(true);
        expect(bool2).to.equal(false);
    })

    it("should revert on diamondCut after stopCuts", async () => {
        let diamond = await PiFiatTokenDiamond.deployed();
        let newFacet = await DiamondCutExample.new();
        let array = [];
        array.push(newFacet.address);
        await truffleAssert.reverts(diamond.diamondCut(array, {from: accounts[1]}));
    })
    
});

function decodeEvent(contract, log) {
	let inputs = contract.events[log.topics[0]].inputs;
	let data = log.data;
	let topics = log.topics.slice(1);
	let event = web3.eth.abi.decodeLog(inputs, data, topics);
	let name = contract.events[log.topics[0]].name;

	return [event, name];
}